<?php

class Calculate
{
    public $x;
    public $y;

    public function setData($x = ' ', $y = ' ')
    {
         $this->x=$x;
        $this->y=$y;
        return $this;
    }

    public function add()
    {
        return $this->x+$this->y;
    }

    public function getData()
    {
        return $this->add();
    }
    public function sub()
    {
        return $this->x-$this->y;
    }

    public function getSub()
    {
        return $this->sub();
    }
    public function mul()
    {
        return $this->x*$this->y;
    }

    public function getMul()
    {
        return $this->mul();
    }
    public function div()
    {
        return $this->x/$this->y;
    }

    public function getDiv()
    {
        return $this->div();
    }
}

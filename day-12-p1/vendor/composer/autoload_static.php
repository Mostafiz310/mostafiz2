<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit55eb26b475a4b1150ead9935685a2439
{
    public static $prefixLengthsPsr4 = array (
        'a' => 
        array (
            'app\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'app\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit55eb26b475a4b1150ead9935685a2439::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit55eb26b475a4b1150ead9935685a2439::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
